/*  Christian Valdez
    masc0385
*/

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayLinearList<E> implements data_structures.LinearListADT<E> {
	
	private int currentSize,maxSize;
	private int front,rear;
	private E[] storage;
	
	public ArrayLinearList(int size){
		maxSize = size;
		currentSize = front = 0;
		rear = 1;
		storage = (E[]) new Object[maxSize];
	}
	
	public ArrayLinearList(){
		this(DEFAULT_MAX_CAPACITY);
	}

	public boolean addFirst(E obj) {
		if(currentSize == maxSize)
			return false;
		storage[front--] = obj;
		if(front == -1) front += maxSize;
		currentSize++;
		return true;
	}

	public boolean addLast(E obj) {
		if(currentSize == maxSize)return false;
		storage[rear++] = obj;
		if(rear == maxSize)rear = 0;
		currentSize++;
		return true;
	}

	public E removeFirst() {
		E dummyVariable;
		if(currentSize == 0)return null;
		if(front == (maxSize -1)){
			dummyVariable = storage[0];
			front = 0;
			currentSize -= 1;
			return dummyVariable;
		}
		dummyVariable = storage[++front];	
		currentSize -= 1;
		return dummyVariable;
	}

	public E removeLast() {
		E dummyVariable;
		if(currentSize == 0)return null;
		if(rear == 0){
			dummyVariable = storage[maxSize - 1];
			rear = maxSize - 1;
			currentSize -= 1;
			return dummyVariable;
		}
		dummyVariable = storage[--rear];
		currentSize -= 1;
		return dummyVariable;
	}

	
	public E remove(E obj) {
		if(currentSize == 0) return null; //If the array is empty
		E dummyVariable;
		int location;
		location = indexOf(obj);
		if(location >= 0){
			dummyVariable = storage[location];
			if(dummyVariable == obj){
				if((location == (rear - 1)) || (rear == 0 && location == (maxSize - 1))){
					removeLast();
					return dummyVariable;
				}
				if(location == (front +1) || (front == (maxSize -1) && location == 0)){
					removeFirst();
					return dummyVariable;
				}
				if(currentSize == 1){
					removeLast();
					return dummyVariable;
				}
				while(location != front){
					if(location == 0){
						storage[location] = storage[(maxSize - 1)];
						location = (maxSize -1);
					}
					else
						storage[location] = storage[--location];
				}
				front++;
				currentSize -= 1;
				return dummyVariable;
			}
		}
		return null;  //Returned if the object is not in the array
	}
	
	//returns index of obj in array or null if not there.
	private int indexOf(E obj){
		int index = (front + 1);
		if(index == maxSize)index = 0;
		int count = 0;
		while(count != currentSize){
			if(index == maxSize)index = 0;
			if(((Comparable<E>)obj).compareTo(storage[index]) == 0)
				return index;
			index++;
			count++;
		}
		return -1;
	}

	public E peekFirst() {
		if(currentSize == 0)return null;
		if(front == (maxSize - 1))
			return storage[0];
		return storage[front + 1];
	}

	public E peekLast() {
		if(currentSize == 0)return null;
		if(rear == 0) return storage[maxSize -1];
		return storage[rear - 1];
	}

	
	public boolean contains(E obj) {
		if(find(obj) != null)
			return true;
		return false;
	}

	public E find(E obj) {
		int index = front + 1;
		if(index == maxSize) index = 0;
		int count = 0;
		while(count!= currentSize){
			if(index == maxSize) index = 0;
			if(((Comparable<E>)obj).compareTo(storage[index]) == 0)
				return storage[index];
			index++;
			count++;
		}
		return null;
	}

	public void clear() {
		currentSize = front = 0;
		rear = 1;
	}

	public boolean isEmpty() {
		if(currentSize == 0) return true;
		return false;
	}

	public boolean isFull() {
		if(currentSize == maxSize) return true;
		return false;
	}

	public int size() {
		return currentSize;
	}
	
	public Iterator<E> iterator() {
		return new IteratorHelper<E>();
	}
	
	private class IteratorHelper<E> implements Iterator<E>{
		
		private int index;
		private long count;
		public IteratorHelper(){
			index = front;
			count = 0;
		}

		@Override
		public boolean hasNext() {
			return count != currentSize;
		}

		@Override
		public E next() {
			if(!hasNext()){
				throw new NoSuchElementException();
			}
			index++;
			count++;
			if(index == maxSize) index = 0;
			return (E) storage[index];
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}

