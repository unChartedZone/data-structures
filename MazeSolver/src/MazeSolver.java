/* Christian Valdez
   masc0385
 */

package prog3;

import data_structures.*;

public class MazeSolver {

    private MazeGrid maze;
    private Queue<GridCell> queue;
    private Stack<GridCell> stack;
    private int endLocation;
    private boolean wasMarked;

    public MazeSolver(int dimension){
        maze = new MazeGrid(this, dimension);
        queue = new Queue();
        stack = new Stack();
        endLocation = dimension - 1;
        wasMarked = false;
    }

    public void mark(){
        GridCell cell;
        maze.getCell(0,0).setDistance(1); //Save this one
        queue.enqueue(maze.getCell(0,0));
        while(!queue.isEmpty()){
            cell = queue.dequeue();
            maze.markDistance(cell);
            enqueueNeighbors(cell);
        }
        wasMarked = true;

    }
    //This method only works with the mark method
    private void enqueueNeighbors(GridCell cell){
        int x = cell.getX();
        int y = cell.getY();
        GridCell top =  maze.getCell(x-1,y);
        GridCell bottem = maze.getCell(x+1,y);
        GridCell left = maze.getCell(x, y-1);
        GridCell right = maze.getCell(x, y+1);

        if(maze.isValidMove(top) && !top.wasVisited()){
            queue.enqueue(top);
            top.setDistance(cell.getDistance() +1);
            maze.markDistance(top);
        }
        if(maze.isValidMove(bottem) && !bottem.wasVisited()){
            queue.enqueue(bottem);
            bottem.setDistance(cell.getDistance() + 1);
            maze.markDistance(bottem);
        }
        if(maze.isValidMove(left) && !left.wasVisited()){
            queue.enqueue(left);
            left.setDistance(cell.getDistance() + 1);
            maze.markDistance(left);
        }
        if(maze.isValidMove(right) && !right.wasVisited()){
            queue.enqueue(right);
            right.setDistance(cell.getDistance() + 1);
            maze.markDistance(right);
        }

    }

    public boolean move(){
        if(!wasMarked) mark();
        GridCell cell = maze.getCell(endLocation,endLocation);
        if(cell.getDistance() == -1) return false;
        while(cell.getDistance() != 1){
            stack.push(cell);
            cell = getMinNeighborCell(cell);
        }
        while(!stack.isEmpty()){
            maze.markMove(stack.pop());
        }
        return true;
    }

    // This method only gets called in the move method
    private GridCell getMinNeighborCell(GridCell cell){
        int x = cell.getX();
        int y = cell.getY();
        GridCell top =  maze.getCell(x-1,y);
        GridCell bottem = maze.getCell(x+1,y);
        GridCell left = maze.getCell(x, y-1);
        GridCell right = maze.getCell(x, y+1);

        if(maze.isValidMove(top) && top.getDistance() < cell.getDistance()){
            return top;
        }
        if(maze.isValidMove(bottem) && bottem.getDistance() < cell.getDistance()){
            return bottem;
        }
        if(maze.isValidMove(left) && left.getDistance() < cell.getDistance()){
            return left;
        }
        if(maze.isValidMove(right) && right.getDistance() < cell.getDistance()){
            return right;
        }
        return null; //This should never happen
    }

    public void reset(){
        wasMarked = false;
        stack.makeEmpty();
        queue.makeEmpty();
    }

    public static void main(String[] args){
        MazeSolver solver = new MazeSolver(25);
    }


}
