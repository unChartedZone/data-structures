
import data_structures.LinearList;


public class Tester {

    public static void main(String[] args){
        LinearList<Integer> list = new LinearList();

        for(int i = 1; i < 101; i++){
            list.addLast(i);
        }

        System.out.println("This is first: " + list.peekFirst());
        System.out.println("This is last: " + list.peekLast());
        System.out.println("This is the size: " + list.size());

        for(int j = 1; j < 101; j++){
            list.remove(j);
        }
        System.out.println("This is first: " + list.peekFirst());
        System.out.println("This is last: " + list.peekLast());
        System.out.println("This is the size: "+ list.size());
    }
}
